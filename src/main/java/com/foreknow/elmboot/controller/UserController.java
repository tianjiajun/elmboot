package com.foreknow.elmboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.foreknow.elmboot.po.User;
import com.foreknow.elmboot.service.UserService;

@RestController
@RequestMapping("/UserController")
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping("/getUserByIdByPass")
	public User getUserByIdByPass(User user) throws Exception{
		return userService.getUserByIdByPass(user);
	}


	@RequestMapping("/saveUser")
	public int saveUser(User user) throws Exception{
		return userService.saveUser(user);
	}
}
