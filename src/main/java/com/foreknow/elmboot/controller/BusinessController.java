package com.foreknow.elmboot.controller;

import java.util.List;

import com.foreknow.elmboot.po.Business;
import com.foreknow.elmboot.po.Cart;
import com.foreknow.elmboot.service.BusinessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("/BusinessController")
public class BusinessController {

	@Autowired
	private BusinessService businessService;

	@RequestMapping("/listBusinessByOrderTypeId")
	public List<Business> listBusinessByOrderTypeId(Business business) throws Exception{
		return businessService.listBusinessByOrderTypeId(business.getOrderTypeId());
	}


	@RequestMapping("/getBusinessById")
	public Business getBusinessById(Business business) throws Exception{
		return businessService.getBusinessById(business.getBusinessId());
	}
}
