package com.foreknow.elmboot.controller;

import com.foreknow.elmboot.po.Orders;
import com.foreknow.elmboot.service.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/OrdersController")
public class OrdersController {
    @Autowired
    private OrdersService ordersService;

    @RequestMapping("/createOrders")
    public int createOrders(Orders orders){
        return ordersService.createOrders(orders);
    }

    @RequestMapping("/getOrderById")
    public Orders getOrdersById(Integer orderId){
        return ordersService.getOrdersById(orderId);
    }

    @RequestMapping("/updateOrderState")
    public int updateOrderState(Orders orders){
        return ordersService.updateOrderState(orders);
    }
}
