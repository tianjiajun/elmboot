package com.foreknow.elmboot.service;

import com.foreknow.elmboot.po.Cart;

import java.util.List;



public interface CartService {

	/**
	 * 查询购物车
	 * @param cart
	 * @return
	 */
	public List<Cart> listCart(Cart cart);

	/**
	 * 添加购物车
	 * @param cart
	 * @return
	 */
	public int saveCart(Cart cart);

	/**
	 * 修改
	 * @param cart
	 * @return
	 */
	public int updateCart(Cart cart);

	/**
	 * 删除购物车
	 * @param cart
	 * @return
	 */
	public int removeCart(Cart cart);
}
