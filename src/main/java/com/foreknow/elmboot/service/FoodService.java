package com.foreknow.elmboot.service;

import com.foreknow.elmboot.po.Food;

import java.util.List;



public interface FoodService {

	/**
	 * 根据商家的Id查询食品列表
	 * @param businessId
	 * @return
	 */
	public List<Food> listFoodByBusinessId(Integer businessId);
}
