package com.foreknow.elmboot.service;

import com.foreknow.elmboot.po.Business;

import java.util.List;



public interface BusinessService {
	/**
	 * 根据orderTypeId查询商家列表
	 * @param orderTypeId
	 * @return
	 */
	public List<Business> listBusinessByOrderTypeId(Integer orderTypeId);

	/**
	 * 根据businessId查询某个商家信息
	 * @param businessId
	 * @return
	 */
	public Business getBusinessById(Integer businessId);
}
