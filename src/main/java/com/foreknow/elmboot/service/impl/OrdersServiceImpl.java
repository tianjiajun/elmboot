package com.foreknow.elmboot.service.impl;

import com.foreknow.elmboot.mapper.CartMapper;
import com.foreknow.elmboot.mapper.OrderDetailetMapper;
import com.foreknow.elmboot.mapper.OrdersMapper;
import com.foreknow.elmboot.po.Cart;
import com.foreknow.elmboot.po.OrderDetailet;
import com.foreknow.elmboot.po.Orders;
import com.foreknow.elmboot.service.OrdersService;
import com.foreknow.elmboot.util.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
@Service
public class OrdersServiceImpl implements OrdersService {
    @Autowired
    private CartMapper cartMapper;

    @Autowired
    private OrdersMapper ordersMapper;

    @Autowired
    private OrderDetailetMapper orderDetailetMapper;

    @Override
    @Transactional
    public int createOrders(Orders orders) {
        //1.查询当前购物中商家的所有食品
        Cart cart = new Cart();
        cart.setUserId(orders.getUserId());
        cart.setBusinessId(orders.getBusinessId());
        List<Cart> cartList = cartMapper.listCart(cart);
        //2.创建订单（要返回一个订单的编号）
        orders.setOrderDate(CommonUtil.getCurrentDate());
        //向Orders表中添加数据
        ordersMapper.saveOrders(orders);
        //添加完数据之后会返回订单的编号
        int orderId = orders.getOrderId();

        //3.批量添加订单的明细
        List<OrderDetailet> list = new ArrayList<>();
        for (Cart c :cartList) {
            OrderDetailet od = new OrderDetailet();
            od.setOrderId(orderId);
            od.setFoodId(c.getFoodId());
            od.setQuantity(c.getQuantity());
            //将明细添加到集合list中
            list.add(od);
        }
        orderDetailetMapper.saveOrderDetailetBatch(list);
        //4.从购物车表中删除相关的食品信息
        cartMapper.removeCart(cart);
        return orderId;
    }
    @Override
    public Orders getOrdersById(Integer orderId) {
        return ordersMapper.getOrdersById(orderId);
    }

    @Override
    public List<Orders> listOrderByUserId(String userId) {
        return ordersMapper.listOrderByUserId(userId);
    }

    @Override
    public int updateOrderState(Orders orders) {
        return ordersMapper.updateOrderState(orders);
    }


}
