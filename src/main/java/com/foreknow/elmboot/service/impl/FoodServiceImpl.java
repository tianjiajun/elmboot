package com.foreknow.elmboot.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.foreknow.elmboot.mapper.FoodMapper;
import com.foreknow.elmboot.po.Food;
import com.foreknow.elmboot.service.FoodService;

@Service
public class FoodServiceImpl implements FoodService{

	@Autowired
	private FoodMapper foodMapper;

	@Override
	public List<Food> listFoodByBusinessId(Integer businessId) {
		return foodMapper.listFoodByBusinessId(businessId);
	}
}
