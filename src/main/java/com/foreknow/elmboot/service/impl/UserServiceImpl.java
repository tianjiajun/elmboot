package com.foreknow.elmboot.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.foreknow.elmboot.mapper.UserMapper;
import com.foreknow.elmboot.po.User;
import com.foreknow.elmboot.service.UserService;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserMapper userMapper;

	@Override
	public User getUserByIdByPass(User user) {
		return userMapper.getUserByIdByPass(user);
	}


	@Override
	public int saveUser(User user) {
		return userMapper.saveUser(user);
	}
}
