package com.foreknow.elmboot.service;


import com.foreknow.elmboot.po.User;

public interface UserService {

	/**
	 * 登录
	 * @param user
	 * @return
	 */
	public User getUserByIdByPass(User user);

	/**
	 * 注册
	 * @param user
	 * @return
	 */
	public int saveUser(User user);
}
