package com.foreknow.elmboot.service;

import com.foreknow.elmboot.po.DeliveryAddress;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface DeliveryAddressService {
    /**
     * 根据用户的id查询地址信息列表
     * @param userId
     * @return
     */
    public List<DeliveryAddress> listDeliveryAddressByUserId(String userId);

    /**
     * 根据daId查询某一个地址信息
     * @param daId
     * @return DeliveryAddress
     */
    public DeliveryAddress getDeliveryAddressById(Integer daId);

    /**
     * 新增地址
     * @param deliveryAddress
     * @return int
     */
    public int saveDeliveryAddress(DeliveryAddress deliveryAddress);

    /**
     * 修改地址信息
     * @param deliveryAddress
     * @return int
     */
    public int updateDeliveryAddress(DeliveryAddress deliveryAddress);


    /**
     * 根据daId删除地址信息
     * @param daId
     * @return
     */
    public int removeDeliveryAddress(Integer daId);

}
