package com.foreknow.elmboot.service;

import com.foreknow.elmboot.po.Orders;

import java.util.List;

public interface OrdersService {
    /**
     * 创建订单
     * @param orders
     * @return
     */
    public int createOrders(Orders orders);

    /**
     * 根据orderId查询订单信息
     * @param orderId
     * @return
     */
    public Orders getOrdersById(Integer orderId);

    /**
     * 根据用户的id查询订单列表
     * @param userId
     * @return
     */
    public List<Orders> listOrderByUserId(String userId);

    /**
     *
     * @param orders
     * @return
     */
    public int updateOrderState(Orders orders);
}
