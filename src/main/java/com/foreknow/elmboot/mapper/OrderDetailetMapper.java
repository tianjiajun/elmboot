package com.foreknow.elmboot.mapper;

import com.foreknow.elmboot.po.OrderDetailet;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface OrderDetailetMapper {
    /**
     * 批量添加明细数据
     * @param list
     * @return
     */
    public int saveOrderDetailetBatch(List<OrderDetailet> list);

    /**
     * 根据订单的id查询明细列表
     * @param orderId
     * @return
     */
    public List<OrderDetailet> listOrderDetailetByOrderId(Integer orderId);
}
