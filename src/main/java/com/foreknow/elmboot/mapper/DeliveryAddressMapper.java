package com.foreknow.elmboot.mapper;

import com.foreknow.elmboot.po.DeliveryAddress;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface DeliveryAddressMapper {
    /**
     * 根据用户的id查询地址信息列表
     * @param userId
     * @return
     */
    @Select("select * from deliveryaddress where userId=#{userId} order by daId")
    public List<DeliveryAddress> listDeliveryAddressByUserId(String userId);

    /**
     * 根据daId查询某一个地址信息
     * @param daId
     * @return DeliveryAddress
     */
    @Select("select * from deliveryaddress where daId=#{daId}")
    public DeliveryAddress getDeliveryAddressById(Integer daId);

    /**
     * 新增地址
     * @param deliveryAddress
     * @return int
     */
    @Insert("insert into deliveryaddress(contactName,contactSex,contactTel,address,userId) values(#{contactName},#{contactSex},#{contactTel},#{address},#{userId})")
    public int saveDeliveryAddress(DeliveryAddress deliveryAddress);

    /**
     * 修改地址信息
     * @param deliveryAddress
     * @return int
     */
    @Update("update deliveryaddress set contactName=#{contactName},contactSex = #{contactSex},contactTel = #{contactTel},address = #{address} where daId=#{daId}")
    public int updateDeliveryAddress(DeliveryAddress deliveryAddress);


    /**
     * 根据daId删除地址信息
     * @param daId
     * @return
     */
    @Delete("delete from deliveryaddress where daId=#{daId}")
    public int removeDeliveryAddress(Integer daId);

}
