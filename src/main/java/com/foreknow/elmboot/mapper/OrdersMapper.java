package com.foreknow.elmboot.mapper;

import com.foreknow.elmboot.po.Orders;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface OrdersMapper {
    /**
     * 添加订单
     * @param orders
     * @return
     */
    @Insert("insert into orders(userId,businessId,orderDate,orderTotal,daId,orderState) values(#{userId},#{businessId},#{orderDate},#{orderTotal},#{daId},0)")
    @Options(useGeneratedKeys = true,keyProperty = "orderId",keyColumn = "orderId")
    public int saveOrders(Orders orders);

    /**
     * 根据orderId查询订单信息
     * @param orderId
     * @return
     */
    public Orders getOrdersById(Integer orderId);

    /**
     *
     * @param orders
     * @return
     */
    @Update("update orders set orderState=1 where orderId=#{orderId} ")
    public int updateOrderState(Orders orders);

    /**
     * 根据用户的id查询订单列表
     * @param userId
     * @return
     */
    public List<Orders> listOrderByUserId(String userId); 
}
