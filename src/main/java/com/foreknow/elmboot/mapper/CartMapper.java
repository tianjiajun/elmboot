package com.foreknow.elmboot.mapper;

import com.foreknow.elmboot.po.Cart;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
@Mapper
public interface CartMapper {
    /**
     *查询购物车信息
     * @param cart
     * @return
     */
    public List<Cart> listCart(Cart cart);

    /**
     * 添加购物车
     * @param cart
     * @return
     */
    @Insert("insert into cart(foodId,businessId,userId,quantity) values(#{foodId},#{businessId},#{userId},1)")
    public int saveCart(Cart cart);

    @Update("update cart set quantity=#{quantity} where foodId=#{foodId} and businessId=#{businessId} and userId=#{userId}")
    public int updateCart(Cart cart);

    /**
     * 删除购物车
     * @param cart
     * @return
     */
    public int removeCart(Cart cart);

}
